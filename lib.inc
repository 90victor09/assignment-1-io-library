section .data
newline: db 10
numbers: db '0123456789'
minus:   db '-'
spaces:  db 0x20, 0x9, 0xA
section .text
 
 
; Принимает код возврата и завершает текущий процесс
exit: ; rdi
    push rcx
    mov rax, 60  ; exit
    syscall
    pop rcx
    ret 

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length: ; rdi
    push rsi
    
    mov rax, -1
.loop:
    inc rax
    cmp byte[rdi + rax], 0
    jne .loop
    
    pop rsi
    ret ; rax

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string: ; rdi
    push rax
    push rdi
    push rsi
    push rdx
    
    call string_length  ; -> rax
    
    mov     rdx, rax    ; length
    mov     rsi, rdi    ; message
    mov     rax, 1      ; print
    mov     rdi, 1      ; stdout
    push rcx
    syscall
    pop rcx
    
    pop rdx
    pop rsi
    pop rdi
    pop rax
    ret

; Принимает код символа и выводит его в stdout
print_char: ; rdi
    push rdi
    sub rsp, 2
    
    mov [rsp], rdi
    mov byte[rsp + 1], 0
    mov rdi, rsp
    call print_string

    add rsp, 2
    pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    push rdi
    mov rdi, newline
    call print_char
    pop rdi
    ret

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    push rax
    push rdi
    push rsi    
    sub rsp, 20
    mov byte[rsp+20], 0
    
    mov rsi, 19
    mov rax, rdi
.div:
    mov	rdx, 0
    mov rdi, 10
    div rdi
    mov cl, byte[numbers + rdx]
    mov byte[rsp + rsi], cl
    dec rsi
    cmp rax, 0
    je .end
    jmp .div

.end:
    inc rsi
    mov rdi, rsi
    add rdi, rsp
    call print_string

    add rsp, 20
    pop rsi
    pop rdi
    pop rax
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    push rdi
    cmp rdi, 0
    jge .print_number
    
    mov rdi, [minus]
    call print_char
    mov rdi, [rsp]
    neg rdi
.print_number:
    call print_uint
    pop rdi
    ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals: ; rdi, rsi
    push rcx
    push rbx
    
    call string_length
    mov rcx, rax
    push rdi
    mov rdi, rsi
    call string_length
    pop rdi
    
    cmp rax, rcx
    jne .fail
    
    mov rcx, 0
.loop:
    mov bl, byte[rdi + rcx]
    cmp bl, byte[rsi + rcx]
    jne .fail
    
    inc rcx
    
    cmp byte[rdi + rcx], 0
    jne .loop
    
    mov rax, 1
    jmp .end
    
.fail:
    mov rax, 0
.end:
    pop rbx
    pop rcx
    ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    push rdi
    push rsi
    push rdx
    
    mov rax, 0    ; read
    mov rdi, 0    ; stdin
    push rax
    mov rsi, rsp  ; buffer on stack
    mov rdx, 1    ; length
    push rcx
    syscall
    pop rcx
    
    pop rax
    
    pop rdx
    pop rsi
    pop rdi
    ret 

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word: ; rdi, rsi
    push rbx

    dec rsi
    mov rbx, 0
    mov rdx, 0
.loop:
    call read_char ; -> rax
    
    cmp rax, 0
    je .not_wide_enough_or_error
    
    cmp al, byte[spaces + 0]
    je .check_end
    cmp al, byte[spaces + 1]
    je .check_end
    cmp al, byte[spaces + 2]
    je .check_end
    
    mov rbx, 1
    mov [rdi + rdx], al
    
    inc rdx
    cmp rsi, rdx
    jl .not_wide_enough_or_error
    jmp .loop

.not_wide_enough_or_error:
    cmp rbx, 1
    je .check_end
    mov rax, 0
    jmp .end

.check_end:
    cmp rbx, 0
    je .loop
    mov byte[rdi + rdx], 0
    mov rax, rdi
.end:
    pop rbx
    ret ; rax, rdx
 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint: ; rdi
    push rcx
    push rsi
    
    mov rax, 0
    mov rdx, 0
.loop:
    mov sil, byte[rdi + rdx]
    sub sil, byte[numbers + 0]
    cmp sil, 9
    ja .check_end_or_fail
    and rsi, 0xFF
    
    push rdx
    mov rcx, 10
    mul rcx
    pop rdx
    
    add rax, rsi
    inc rdx
    jmp .loop
    
.check_end_or_fail:
    cmp rdx, 0
    ja .end
    mov rdx, 0
.end:
    pop rsi
    pop rcx
    ret




; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int: ; rdi
    push rbx
    
    mov bl, byte[rdi]
    sub bl, byte[minus]
    cmp bl, 0
    je .skip_minus
.handle_number:
    call parse_uint
    cmp rdx, 0
    je .end
    
    cmp bl, 0
    je .handle_minus
    
    jmp .end
    
.skip_minus:
    inc rdi
    jmp .handle_number
    
.handle_minus:
    inc rdx
    neg rax
.end:
    pop rbx
    ret 

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy: ; rdi, rsi, rdx
    push rbx ; bl
    push rdx
    
    push rsi
    mov rsi, rdi
    call string_length
    pop rsi

    cmp rdx, rax
    jl .not_wide_enough
    
    mov rdx, rax
    mov rax, 0
.loop:
    mov bl, byte[rdi + rax]
    mov byte[rsi + rax], bl
    cmp rax, rdx
    je .end
    inc rax
    jmp .loop
    
.not_wide_enough:
    mov rax, 0
.end:
    pop rdx
    pop rbx
    ret
